---
title: '“I find beauty in art that destroys itself”'
subtitle: "An interview with Marcus Woods"
description: >
  “I’m a perfectionist with my music,” states Ryan Cullen, the man behind the moniker of Marcus Woods, as we speak to each other remotely in the comfort of our bedrooms. “I won’t be happy with it unless every single detail is there. I need everything to keep pacing forward.”
date: "2020-10-01"
slug: "marcus-woods-self-portrait"
---

![](cover.png)

{{< section "begin" >}}

“I’m a perfectionist with my music,” states Ryan Cullen, the man behind the
moniker of Marcus Woods, as we speak to each other remotely in the comfort of
our bedrooms.
“I won’t be happy with it unless every single detail is there.
I need everything to keep pacing forward.”

‘Pacing forward’ perfectly captures both Woods’s creative process, and how the
Dublin-based artist overcame adversity during the production of Self-Portrait.
Woods’s inaugural full-length release is a ten-track album featuring
collaborations from Silent Ghost and Cork-based Actualacid, painting a
relatable picture of an anxious mind and a constant struggle with
self-identity.
Starting out as a lo-fi producer, Woods’s musical style has since evolved into 
an ethereal, ambient affair.
“I stopped doing lo-fi because I felt it was too monotonous.
I felt I didn’t have freedom within that genre to experiment.
Reaching out and listening to new artists and producers really inspired me.”

{{< section "end" >}}


{{< section "begin" >}}

Self-Portrait marks a focal point in Woods’s sound.
16 months of work, many revisions, and a blizzard of artistic destruction 
shaped the final iteration of Self-Portrait.
“My girlfriend always calls me impulsive,” a sentiment accepted by Woods 
himself.
“That helps stem inspiration because I’m open to new things.”
As a full-time media student stuck in a job he loathed, the breadth of Woods’s 
personal responsibilities soon took hold of his life.
“All of a sudden everything burnt out.
I came crashing down,” explains Woods. 
“I remember walking to college one day and I had two cans of Monster in my 
bag.
One of the cans opened with my laptop in the bag.
The whole laptop crashed for three days.
At first I was scared because I had so much work on the laptop and I didn’t 
back it up.”

Luckily, the laptop showed signs of life and he could recover his work.
In a moment of fleeting madness, however, Woods consciously deleted everything he’d done.
“I got used to that feeling of ‘Everything’s gone, I have to start fresh.’
I went on and skimmed through the projects, and I just deleted them and didn’t 
think about it.
It just sort of happened.”
Asking if he regretted what he did in hindsight, Woods confidently replied, “I 
look back now and I’m really happy that I was able to start fresh.
I find beauty in art that destroys itself.
My way of working is when I’m working on my next project, I need to destroy 
what was before and build off that again.”

The final product is a constantly evolving soundscape of lush synths and
samples, but that wasn’t the intention at the start.
“There were some ambient elements but most of it was processed drums and 
samples, loud clipping and hard distorted noises,” Woods says about the 
initial version of the album, the remnants of which can be heard in the 
album’s second track and first single, ‘Repose Tactics’.
“I took a step back and said hang on, this isn’t me at the moment.
It would’ve been a very angry album.”

Not only did the final sound go through drastic change, but so did the final 
form and structure.
“There were a ton of people I was trying to get on this project and had plans 
to make tracks with,” name-dropping artists such as Fomorian Vein and 
Happyalone.
Self-Portrait almost manifested as a twenty-track album with half the tracks 
bringing in other talent from the Irish music scene.
“But then it came to me: Don’t drop a twenty-track album because nobody will 
listen to it.
If I dropped a twenty-tracker it easily would’ve been nearly two hours,” Woods 
says about the album which comes in at just under 45 minutes in length.
“I’m still working with people at the moment and hope to drop a few singles or 
an EP, but I think 7-10 collaborations and a 20-track album wouldn’t have 
worked.
I guess I’ll never know.”

{{< section "end" >}}

{{< section "begin" >}}

Woods and his long-time friend and musical collaborator, Silent Ghost, spent 
most of their time in the studio from December to January.
Silent Ghost worked on polishing the final mix and master of each track, 
though the two spent a lot of time recording pieces of music together, too.
After asking how ‘BCBCF2’ came about, Woods told me “It stemmed from 
absolutely nowhere.
We have this amazing chemistry.
While we make completely different music, we meet at that middle point and we 
know exactly what we need to do.”
The track took less than five minutes to produce, with Silent Ghost manning a 
guitar hooked up to a bunch of reverb and loop pedals, and Woods leveraging 
Kontakt by Native Instruments to create a modulated rising synth with a pad 
underneath.

Previously collaborating with Actualacid to produce ‘Smith’ earlier this year, 
it was natural for Woods to bring him on the record.
“Getting Actualacid on the record was really cool.
We started working on Smith around this time last year, and we finally got it 
finished a few months ago right before the release.”
The track features a sample of ‘Imagine’ by Ariana Grande processed by 
Actualacid, which formed the base of the track.
“The way he pitched it was like this sheer screaming.
It was strange.
I don’t know what kind of emotion we were trying to evoke.
It was this extremely unsettling moment mid-way through the album.
It comes straight in your face with no warning and no fading.
You just hear this screaming sample of Ariana Grande and this and this empty, 
dark synth and then straight into ‘BCBCF2’.”

Self-Portrait is more than just a collection of music as we see collaborations 
with a number of visual artists on the project.
With the successful funding of Self-Portrait’s vinyl release on Kickstarter, 
Woods explains the idea behind the colouring book he’s sending out with each 
order.
“I saw what The 1975 did with their music videos.
They reached out to different independent visual artists who responded to each 
track.
I thought this is a really cool idea, why don’t I reach out to Irish artists 
and get them to draw their response to each track?
So, for each track there’s a drawing by an Irish artist.”

{{< section "end" >}}

{{< section "begin" >}}

The visual element extends further than a colouring book, with Woods gearing 
up to perform a live show next year at Whelan’s alongside Silent Ghost.
“We’re planning a lot of lights, visuals, smoke machines and strobes.
We want people to walk in and walk out and think ‘What the fuck did I just
see?’
I want people to not know what to think about it.
I don’t want it to be myself and Silent Ghost on the stage doing bits.
I want this to be an experience.”
In the space of time after the album was mastered and ready to be unveiled, 
Woods collaborated with visual artists Colm Cahalane of Irish collective 
Hausu, and Demon Sanctuary who produced a visual response to The 1975’s Notes 
On A Conditional Form to create the music videos for ‘Repose Tactics’ and 
‘BCBCF2’ respectably.

As a global pandemic rages on and continues to affect every aspect of our 
society, I was interested in Woods’s opinion on how the Irish music scene will 
progress into the future.
“The artists are doing the most they can, if not, they’re pulling more than 
they should be.
It’s great to see, but it’s on the industry to step up their game and help us 
progress.
People compare us to the UK all the time, but can we really be compared to 
them?
The music is above par, the music isn’t the thing that’ll hold us back. 
Ireland and the government need to focus on helping Irish music come to 
fruition and be on par with the UK.”

Wrapping up our conversation and awaiting the screening of RTÉ’s documentary 
on Irish hip-hop, I asked Woods if he had any words of wisdom for any 
newcomers to the Irish music scene.
“I know it’s cliché, but you have to be yourself.
Don’t try to copy something someone else is doing, because it’ll be out of 
style by the time you put it out.
If you’re going to make it, you want to create that sense of individuality.
At the same time, if you don’t feel confident releasing something, don’t 
release it.
You don’t have to be put under this pressure.”

Self-Portrait is out now on all major streaming platforms, as well as
[on Bandcamp][bandcamp] where you can buy and download the highest-quality 
digital release.
As of the time of publishing, Marcus Woods is getting ready to perform a live 
show at Whelan’s, Dublin, in April 2021.

{{< section "end" >}}

{{< section "start" >}}
_This interview was [originally published][source] in the University Express's music section._ 
{{< section "end" >}}

[source]: https://uccexpress.ie/i-got-used-to-this-feeling-of-everythings-gone-marcus-woods/
[cdon]: https://cathaldon.netsoc.cloud/
[bandcamp]: https://wxxdsy.bandcamp.com/album/self-portrait
